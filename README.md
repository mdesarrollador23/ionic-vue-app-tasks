<div align="center" width="100vw"> <img width="400" height="800" src="https://i.ibb.co/YTH657m/Whats-App-Image-2021-07-04-at-11-59-12-PM.jpg" /> </div>
# vue2

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
ionic build
```

### Lints and fixes files
```
More https://ionicframework.com/
```
