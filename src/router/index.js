import { createRouter, createWebHistory } from '@ionic/vue-router';

const routes = [
  {
    path: '/',
    component: () => import('@/views/Home'),
    redirect: { name: 'tasks' },
    children: [
      {
        path: '/tasks',
        name: 'tasks',
        component: () => import('@/views/Tasks')
      },
      {
        path: '/create',
        name: 'create',
        component: () => import('@/views/Create')
      },
      {
        path: '/gitlab',
        name: 'gitlab',
        component: () => import('@/views/Gitlab')
      },
    ]
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
