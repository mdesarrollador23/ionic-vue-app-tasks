const state = () => ({
    tasks: []
});

const getters = {
    allTasks: state => {
        return state.tasks
    }
};

const actions = {
    addTask({ commit }, payload) {
        commit('saveAllTasks', payload)
    },
    deleteTask({ commit }, id) {
        commit('deleteTask', id);
    }
};

const mutations = {
    saveAllTasks(state, payload) {
        payload.id = state.tasks.length + 1;
        state.tasks.unshift(payload)
    },
    deleteTask(state, id) {
        state.tasks = state.tasks.filter(u => u.id !== id);
    }
}

export default {
    namespaced: true,
    state, getters, actions, mutations
}