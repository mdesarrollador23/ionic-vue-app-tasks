import { createStore } from 'vuex'
import tasksModule from './module/tasks'

const store = createStore({
    modules: {
        tasks: tasksModule
    }
});

export default store